import 'package:flutter/material.dart';

class ListView2Screen extends StatelessWidget {
  final options = const ['megaman', 'Metal', 'Super', 'Final'];

  const ListView2Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('ListView2'),
          /* Sombra esta basada en elevation */
          elevation: 0,
          /* Cambiar color del appBar */
          backgroundColor: Colors.indigo,
        ),
        /* Se crea el ListView para crear una Lista */
        body: ListView.separated(
            itemBuilder: (context, index) => ListTile(
                  /* Titulo del ListTile */
                  title: Text(options[index]),
                  /* Poner icon al final del item */
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.indigo,
                  ),
                  /* Funcion onTap para click */
                  onTap: () {
                    final game = options[index];
                    print(game);
                  },
                ),
            separatorBuilder: (_, __) => const Divider(),
            itemCount: options.length));
  }
}
