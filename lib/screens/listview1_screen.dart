import 'package:flutter/material.dart';

class ListView1Screen extends StatelessWidget {
  final options = const ['megaman', 'Metal', 'Super', 'Final'];

  const ListView1Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('ListView1'),
        ),
        /* Se crea el ListView para crear una Lista */
        body: ListView(
          /* El listiview siempre va tener un children */
          children: [
            /* Tomamos una copia del objeto de string y con el map transformamos en listaTile e imprimos el nombre del juego en un text */
            ...options
                .map((gameTitle) => ListTile(
                      title: Text(gameTitle),
                      /* Trailing para poner icon al final de lista */
                      trailing: const Icon(Icons.arrow_forward_ios),
                      /* Los  ListTile tiene un metodo que reciben una funcion*/
                    ))
                .toList()
          ],
        ));
  }
}
