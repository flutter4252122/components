import 'package:components/router/app_routes.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Componentes en Flutter'),
          elevation: 0,
        ),
        body: ListView.separated(
            itemBuilder: (context, index) => ListTile(
                  /* Icono al comienzo del item */
                  leading: Icon(AppRoutes.menuOptions[index].icon),
                  title: Text(AppRoutes.menuOptions[index].name),
                  /* Click en el item del listile */
                  onTap: () {
                    /* Definir la ruta */
                   /*  final route = MaterialPageRoute(builder: (context) => const ListView1Screen());
                    Navigator.push(context, route); */
                    /* Se pone el nombre de la ruta creada */
                    Navigator.pushNamed(context, AppRoutes.menuOptions[index].route);
                  },
                ),
            separatorBuilder: (context, index) => const Divider(),
            itemCount: AppRoutes.menuOptions.length));
  }
}
