import 'package:components/router/app_routes.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      /* QUITAR EL DEBUG DEL NAVBAR */
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      /* home: const ListView2Screen(), */
      /* initialRoute para iniciar en una ruta especifica */
      initialRoute: AppRoutes.inicialRoute,
      /* Rutas de la aplicacion */
      routes: AppRoutes.getAddRoutes(),
      /* Si no esta definida la ruta */
      onGenerateRoute: (settings) => AppRoutes.onGenerateRoute(settings),
    );
  }
}
