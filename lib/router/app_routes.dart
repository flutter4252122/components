import 'package:flutter/material.dart';
/* IMPORTACIONES DE LOS SCREENS */
import 'package:components/screens/screens.dart';
/* IMPORTACIONES DE LOS MODELOS */
import 'package:components/models/models.dart';

class AppRoutes {
  /* Ruta inicial */
  static const inicialRoute = 'home';

  static final menuOptions = <MenuOption>[
    /* BORRAR HOME */
    /* HOME */
    MenuOption(
        route: 'home',
        name: 'Home Screen',
        screen: const HomeScreen(),
        icon: Icons.home),
        /* LISTVIEW1 */
    MenuOption(
        route: 'listView1',
        name: 'listView1 Screen',
        screen: const ListView1Screen(),
        icon: Icons.list_sharp),
        /* LISTVIEW2 */
    MenuOption(
        route: 'listView2',
        name: 'listView2 Screen',
        screen: const ListView2Screen(),
        icon: Icons.list_alt_outlined),
        /* CARD */
    MenuOption(
        route: 'card',
        name: 'Card Screen',
        screen: const CardScreen(),
        icon: Icons.card_membership),
        /* ALERTS */
        MenuOption(
        route: 'alert',
        name: 'Alert Screen',
        screen: const AlertScreen(),
        icon: Icons.add_alert_outlined)
  ];


  static  Map<String, Widget Function(BuildContext)> getAddRoutes(){

    Map<String, Widget Function(BuildContext)> appRoutes = {};

    for (final option in menuOptions) {
      /* se retorna el nombre de la ruta y el archivo screen */
      appRoutes.addAll({option.route: (BuildContext context) => option.screen});
    }
    return appRoutes;
  }



  /* RUTAS2 */
  /* static Map<String, Widget Function(BuildContext)> routes = {
    'home': (BuildContext context) => const HomeScreen(),
    'listView1': (BuildContext context) => const ListView1Screen(),
    'listView2': (BuildContext context) => const ListView2Screen(),
    'alert': (BuildContext context) => const AlertScreen(),
    'card': (BuildContext context) => const CardScreen()
  }; */

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    /* Si no esta creada alguna ruta ingresa a el screen de Alert */
    return MaterialPageRoute(builder: (context) => const AlertScreen());
  }
}
